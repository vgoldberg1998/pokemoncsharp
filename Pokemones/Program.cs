﻿using System;

namespace Pokemones

{
    public partial class Pokedex
    {
        static void Main(string[] args)
        {
            while (getMenu()) ;

            Console.WriteLine("Se termino la Pokedex");
        }

        private static bool getMenu()
        {
            bool isMenu = true;
            Console.Clear();
            Console.WriteLine("Menu Pokemon");
            Console.WriteLine("1.- Cargar Pokemones Json");
            Console.WriteLine("2.- Listar Pokemones.");
            Console.WriteLine("3.- Duelo");
            Console.WriteLine("4.- Buscador");
            Console.WriteLine("0.- Salir");

            string opcion = Console.ReadLine();
            switch (opcion) {
                case "1":
                    readJsonFile();
                    break;
                case "2":
                    listPokemon();
                    break;
                case "3":
                    getDuelo();
                    break;
                case "4":
                    getPokemonNombre();
                    break;

                case "0":
                    isMenu = false;
                    break;
                default:
                    Console.WriteLine("Opcion invalida");
                    Console.ReadKey();
                    break;
            }

            return isMenu;
        }

       
    }
}
