﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class PokemonDAL
    {
        static List<Pokemon> pokemones = new List<Pokemon>();

        public void saveList(List<Pokemon> pokes) {
            pokemones = pokes;
        }
        public void addPokemon(Pokemon p) {
            pokemones.Add(p);
        }

        public List<Pokemon> getPokemones() {
            return pokemones;
        }

        public List<Pokemon> getPokemonbyNombre(string nombrePokemon) {
            List<Pokemon> nombreString=new List<Pokemon>();
            foreach(Pokemon p in pokemones)
            {
                //if (p.name.Equals(nombrePokemon))nombreString.Add(p);
                if (p.name.IndexOf(nombrePokemon) != -1) {
                    nombreString.Add(p);
                }

                

            }

            return nombreString;
           
            
        }


    }
}
